package com.zeroqualitygames.pong.resources;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class BufferedImageLoader {

	public BufferedImageLoader() {
		// TODO Auto-generated constructor stub
	}
	
	public BufferedImage loadImage(String name) {
		URL url = this.getClass().getResource(name);
		try {
			return ImageIO.read(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Unable to load the image: " + name);
		}
		return null;
	}

}
