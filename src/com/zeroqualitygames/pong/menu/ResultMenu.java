package com.zeroqualitygames.pong.menu;

import java.awt.Graphics2D;

import com.zeroqualitygames.pong.Game;
import com.zeroqualitygames.pong.InputManager;

public class ResultMenu extends Menu {
	private int highestScorePlayer;
	private Game game;
	private InputManager input;

	public ResultMenu(Game game, InputManager input) {
		super("ResultMenu");
		this.game = game;
		this.input = input;
		Game.highestScore = Math.max(Game.leftScore, Game.rightScore);
		Game.lowestScore = Math.min(Game.leftScore, Game.rightScore);
		
		if (Game.leftScore > Game.rightScore)
			highestScorePlayer = 1;
		else
			highestScorePlayer = 2;
	}
	
	public void tick() {
		input.tick();
		if (input.enter.clicked)
			game.setMenu(null);
		if (input.escape.clicked || input.back.clicked)
			game.setMenu(new StartMenu(game, input));
	}
	
	public void render(Graphics2D g) {
		g.drawString("The highest Score was " + Game.highestScore + " by player " + highestScorePlayer, 50, 50);
		g.drawString("The longest rally went for " + Game.highestRally + " turns while the shortest went for " + Game.lowestRally, 50, 100);
	}

}
