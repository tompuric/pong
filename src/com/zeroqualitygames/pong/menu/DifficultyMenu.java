package com.zeroqualitygames.pong.menu;

import java.awt.Graphics2D;

import com.zeroqualitygames.pong.Game;
import com.zeroqualitygames.pong.InputManager;

public class DifficultyMenu extends Menu {
	private Game game;
	private InputManager input;
	private final String[] options = {"Easy", "Medium", "Hard"};
	private int selected = 0;

	public DifficultyMenu(Game game, InputManager input) {
		super("DifficultyMenu");
		this.game = game;
		this.input = input;
	}
	
	public void tick() {
		input.tick();
		
		if (input.down.clicked) selected++;
		if (input.up.clicked) selected--;
		
		if (selected < 0)
			selected = options.length - 1;
		if (selected > options.length - 1)
			selected = 0;
		
		if (input.escape.clicked || input.back.clicked)
			game.setMenu(new StartMenu(game, input));
		
		if (input.enter.clicked) {
			game.setMenu(null);
			Game.DIFFICULTY = selected + 1;
			Game.playerOne.init();
			Game.playerTwo.init();
		}
	}
	
	public void render(Graphics2D g) {
		int spacing = 40;
		for (int i = 0; i < options.length; i++) {
			g.drawString(options[i], 50, (2 + i) * spacing);
		}
		g.drawString(">", 40, (2 + selected) * spacing);
		g.drawImage(image, Game.WIDTH - image.getWidth() - 10, Game.HEIGHT - image.getHeight() - 10, null);
	}

}
