package com.zeroqualitygames.pong.menu;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.zeroqualitygames.pong.resources.BufferedImageLoader;

public class Menu {
	public String name;
	public BufferedImage image = new BufferedImageLoader().loadImage("pong.png");
	
	public Menu(String name) {
		this.name = name;
	}
	
	public void tick() {
		
	}
	
	public void render(Graphics2D g) {
	}

}
