package com.zeroqualitygames.pong.menu;

import java.awt.Font;
import java.awt.Graphics2D;

import com.zeroqualitygames.pong.Game;
import com.zeroqualitygames.pong.InputManager;
import com.zeroqualitygames.pong.sound.Sound;

public class StartMenu extends Menu {
	private final String[] options = {"Start", "Exit", "Difficulty", "Statistics"};
	private int selected = 0;
	
	private Game game;
	private InputManager input;
	
	public StartMenu(Game game, InputManager input) {
		super("StartMenu");
		this.game = game;
		this.input = input;
	}
	
	public void tick() {
		input.tick();
		
		if (input.down.clicked) selected++;
		if (input.up.clicked) selected--;
		
		if (selected < 0) 
			selected = options.length - 1;
		if (selected > options.length - 1) 
			selected = 0;
		
		if (input.enter.clicked) {
			switch (selected) {
				case 0:
					Sound.hit.play();
					game.setMenu(null);
					break;
				case 1:
					System.exit(0);
					break;
				case 2:
					Sound.hit.play();
					game.setMenu(new DifficultyMenu(game, input));
					break;
				case 3:
					Sound.hit.play();
					game.setMenu(new ResultMenu(game, input));
					break;
			}
		}
	}
	
	public void render(Graphics2D g) {
		g.setFont(new Font("Arial", Font.BOLD, 24));
		g.drawString("Pong", 15, 35);
		g.setFont(new Font("Arial", Font.PLAIN, 16));
		int spacing = 40;
		for (int i = 0; i < options.length; i++) {
			g.drawString(options[i], 50, (2 + i) * spacing);
		}
		g.drawString(">", 40, (2 + selected) * spacing);
		g.drawImage(image, Game.WIDTH - image.getWidth() - 10, Game.HEIGHT - image.getHeight() - 10, null);
	}

}
