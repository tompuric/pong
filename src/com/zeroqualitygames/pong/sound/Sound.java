package com.zeroqualitygames.pong.sound;

import java.applet.Applet;
import java.applet.AudioClip;

public class Sound {
	
	public static final Sound hit = new Sound("hit.wav");
	public static final Sound die = new Sound("die.wav");
	
	private AudioClip sound;
	private String name;

	public Sound(String name) {
		this.name = name;
		try {
			sound = Applet.newAudioClip(this.getClass().getResource(name));
		} catch (Exception e) {
			System.out.println("Unable to load audio file: "+ name);
		}
	}
	
	public static void load() {
		// initialise sound files
	}
	
	public void play() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					sound.play();
				}
				catch (Exception e) {
					System.out.println("Unable to play the audio file: " + name);
				}
			}
			
		}).start();
	}
	
	public void loop() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					sound.loop();
				}
				catch (Exception e) {
					System.out.println("Unable to play the audio file: " + name);
				}
			}
			
		}).start();
	}
	
	public void stop() {
		if (sound != null)
			sound.stop();
	}

}
