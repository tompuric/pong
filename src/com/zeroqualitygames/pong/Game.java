package com.zeroqualitygames.pong;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import com.zeroqualitygames.pong.entity.Ball;
import com.zeroqualitygames.pong.entity.Entity;
import com.zeroqualitygames.pong.entity.Player;
import com.zeroqualitygames.pong.menu.Menu;
import com.zeroqualitygames.pong.menu.StartMenu;

public class Game extends Canvas implements Runnable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 500;
	public static final int HEIGHT = 300;
	
	private Thread gameThread;
	private boolean running = false;
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private Graphics2D g = (Graphics2D) image.getGraphics();
	
	private static ArrayList<Entity> entities = new ArrayList<Entity>();
	private InputManager input = new InputManager(this);
	public Menu menu = new Menu("Menu");
	public static Player playerOne;
	public static Player playerTwo;
	private Ball ball = new Ball();
	public static int leftScore, rightScore, rally;
	
	public static int lowestRally, highestRally;
	public static int lowestScore, highestScore;
	public static int topPlayer;
	
	public static final int EASY = 1;
	public static final int MEDIUM = 2;
	public static final int HARD = 3;
	
	public static int DIFFICULTY = EASY;
	
	public Game() {
		
		playerOne = new Player(1, this);
		playerTwo = new Player(2, this);
		
		entities.add(playerOne);
		entities.add(playerTwo);
		
		newGame();
		setMenu(new StartMenu(this, input));
	}
	
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	
	public void newGame() {
		for (Entity e : entities) {
			e.init(this);
		}
		leftScore = 0;
		rightScore = 0;
		rally = 0;
	}

	@Override
	public void run() {
		int sleepTime = 15;
		while (running) {
			update();
			render();
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void update() {
		if (menu != null) {
			menu.tick();
			
			ball.refresh();
		}
		else {
			input.tick();
			
			ball.tick();
			playerOne.tick();
			playerTwo.tick();
			
			if (ball.removed) {
				ball = new Ball();
				rally = 0;
			}

			if (input.escape.clicked || input.pause.clicked)
				setMenu(new StartMenu(this, input));
			
		}
	}
	
	private void render() {
		BufferStrategy strategy = getBufferStrategy();
		if (strategy == null) {
			createBufferStrategy(3);
			requestFocus();
			return;
		}
		
		paintComponent(getGraphics2D());
		
		Graphics g = strategy.getDrawGraphics();
		g.drawImage(image, 0, 0, WIDTH, HEIGHT, this);
		g.dispose();
		strategy.show();
	}
	
	private void paintComponent(Graphics2D g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, WIDTH - 1, HEIGHT - 1);
		
		
		if (menu != null) {
			menu.render(g);
		}
		else {
			ball.render(g);
			playerOne.render(g);
			playerTwo.render(g);
			
			g.setFont(new Font("Arial", Font.PLAIN, 12));
			g.drawString(leftScore + "  :  " + rightScore, WIDTH/2 - 20, 15);
			g.drawString("" + rally, WIDTH/2 - 10, HEIGHT - 10);
		}
	}
	
	public static ArrayList<Entity> getEntities() {
		return entities;
	}
	
	private Graphics2D getGraphics2D() {
		return g;
	}
	
	public void addNotify() {
		super.addNotify();
		start();
	}
	
	public void start() {
		if (!running && gameThread == null) {
			running = true;
			gameThread = new Thread(this);
			gameThread.start();
		}
	}

	public void stop() {
		if (running) {
			running = !running;
		}
	}
}
