package com.zeroqualitygames.pong;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import com.zeroqualitygames.pong.Game;

public class InputManager implements KeyListener {
	
	public class Key {
		public boolean pressed = false;
		public boolean clicked = false;
		public int pressCount = 0;
		public int absorbs = 0;
		
		public Key() {
			keys.add(this);
		}
		
		public void toggle(boolean pressed) {
			this.pressed = pressed;
			if (pressed)
				pressCount++;
		}
		
		public void tick() {
			if (absorbs < pressCount) {
				absorbs++;
				clicked = true;
			}
			else
				clicked = false;
			
		}
	}
	
	private int player = 0;
	public ArrayList<Key> keys = new ArrayList<Key>();
	
	public Key up = new Key();
	public Key down = new Key();
	
	public Key escape = new Key();
	public Key enter = new Key();
	public Key pause = new Key();
	public Key back = new Key();

	public InputManager(Game game) {
		game.addKeyListener(this);
	}
	
	public InputManager(Game game, int player) {
		game.addKeyListener(this);
		this.player = player;
	}
	
	public void tick() {
		for (Key k : keys)
			k.tick();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		toggle(e, true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		toggle(e, false);
	}
	
	public void toggle(KeyEvent e, boolean pressed) {
		switch (player) {
			case 1:
				if (e.getKeyCode() == KeyEvent.VK_W) 		up.toggle(pressed);
				if (e.getKeyCode() == KeyEvent.VK_S) 		down.toggle(pressed);
				break;
			case 2:
				if (e.getKeyCode() == KeyEvent.VK_UP)		up.toggle(pressed);
				if (e.getKeyCode() == KeyEvent.VK_DOWN) 	down.toggle(pressed);
				break;
			default:
				if (e.getKeyCode() == KeyEvent.VK_W) 		up.toggle(pressed);
				if (e.getKeyCode() == KeyEvent.VK_S) 		down.toggle(pressed);
				if (e.getKeyCode() == KeyEvent.VK_UP)		up.toggle(pressed);
				if (e.getKeyCode() == KeyEvent.VK_DOWN) 	down.toggle(pressed);
				break;
		}
		
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) 			escape.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_ENTER) 			enter.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_P) 				pause.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) 		back.toggle(pressed);
	}

}
