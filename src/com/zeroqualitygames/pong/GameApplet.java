package com.zeroqualitygames.pong;

import java.applet.Applet;
import java.awt.BorderLayout;

import com.zeroqualitygames.pong.sound.Sound;

public class GameApplet extends Applet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Game game = new Game();
	
	public void init() {
		setLayout(new BorderLayout());
		add(game, BorderLayout.CENTER);
		Sound.load();
	}
	
	public void start() {
		game.start();
	}
	
	public void stop() {
		game.stop();
	}
}
