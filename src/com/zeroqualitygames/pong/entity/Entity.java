package com.zeroqualitygames.pong.entity;

import java.awt.Rectangle;
import java.util.ArrayList;

import com.zeroqualitygames.pong.Game;
import com.zeroqualitygames.pong.sound.Sound;

public class Entity {
	public float x;
	public float y;
	public int width;
	public int height;
	public float vx;
	public float vy;
	
	public Game game;
	public Rectangle rect;
	public boolean removed = false;
	public ArrayList<Entity> entities;

	public Entity() {
	}
	
	public void init(Game game) {
		this.game = game;
	}
	
	public void remove() {
		removed = true;
	}
	
	public void tick() {
		
	}
	
	public boolean outOfBounds() {
		if (y < 0) {
			y = 0;
			return true;
		}
		if (y > Game.HEIGHT - height) {
			y = Game.HEIGHT - height;
			return true;
		}
		return false;
	}
	
	public boolean checkCollision() {
		entities = Game.getEntities();
		for(Entity e : entities) {
			
			if (e == this)
				continue;
			if (rect.intersects(e.rect) && e instanceof Player) {
				Sound.hit.play();
				Player p = (Player) e;
				if (p.player == 1)
					x = e.x + e.width;
				if (p.player == 2)
					x = e.x - width;
				vx = -vx;
				Game.rally++;
				return true;
			}
			
		}
		rect.setLocation((int)x, (int)y);
		return false;
	}
	
	public void move (float vx, float vy) {
		if (vx != 0 && vy != 0)
			return; // cannot move in both directions
		x += vx;
		y += vy;
		rect.setLocation((int)x, (int)y);
	}

}
