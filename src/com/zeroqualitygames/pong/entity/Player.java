package com.zeroqualitygames.pong.entity;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import com.zeroqualitygames.pong.Game;
import com.zeroqualitygames.pong.InputManager;

public class Player extends Entity {
	public int player;
	private InputManager input;
	private int speed = 5;

	public Player(int player, Game game) {
		this.player = player;
		input = new InputManager(game, player);
		width = 10;
		init();
		
	}
	
	public void init() {
		switch (Game.DIFFICULTY) {
			case Game.EASY:
				height = 70;
				speed = 5;
				break;
			case Game.MEDIUM:
				height = 60;
				speed = 4;
				break;
			case Game.HARD:
				height = 50;
				speed = 3;
				break;
		}
		
		switch (player) {
			case 1:
				x = width;
				break;
			case 2:
				x = Game.WIDTH - width*2;
				break;
			default:
				System.err.println("Not allowed");
				return;
		}
		y = 50;
		rect = new Rectangle((int)x, (int)y, width, height);
	}
	
	public void tick() {
		rect.setLocation((int)x, (int)y);
		input.tick();
		
		if (input.down.pressed)
			vy = speed;
		else if (input.up.pressed)
			vy = -speed;
		else
			vy = 0;
		
		y += vy;
		
		outOfBounds();
	}
	
	public void render(Graphics2D g) {
		g.fillRect((int)x, (int)y, width, height);
	}
	
	@Override
	public String toString() {
		return "player";
	}

}
