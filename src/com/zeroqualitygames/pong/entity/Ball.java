package com.zeroqualitygames.pong.entity;

import java.awt.Font;
import java.awt.Rectangle;
import java.util.Random;
import java.awt.Graphics2D;

import com.zeroqualitygames.pong.Game;
import com.zeroqualitygames.pong.sound.Sound;

public class Ball extends Entity {
	private Random random = new Random();
	private long startTime;
	private final int MAX_VELOCITY = 7;
	private final float incr = 0.2f;
	
	public Ball() {
		width = 10;
		height = 10;
		switch (random.nextInt(2)) {
			case 0:
				vx = -2;
				break;
			case 1:
				vx = 2;
				break;
		}
		vy = (float) (3 + Math.random() * 3);
		refresh();
		rect = new Rectangle((int)x, (int)y, width, height);
	}
	
	public void refresh() {
		startTime = System.currentTimeMillis();
		x = Game.WIDTH/2;
		y = Game.HEIGHT/2;
	}
	
	public void tick() {
		if (System.currentTimeMillis() - startTime > 3000) {
			move(0, vy);
			if (outOfBounds()) {
				vy = -vy;
			}
			
			move(vx, 0);
			if (checkCollision())
				incrementSpeed();
			
			if (checkGame()) {
				Sound.die.play();
				updateScores();
				remove();
			}
		}
	}
	
	private void updateScores() {
		if (Game.leftScore + Game.rightScore == 1) {
			Game.lowestRally = Game.rally;
		}
		if (Game.rally > Game.highestRally) {
			Game.highestRally = Game.rally;
		}
		if (Game.rally < Game.lowestRally) {
			Game.lowestRally = Game.rally;
		}
		
	}
	
	public boolean checkGame() {
		if (x < 0) {
			Game.rightScore++;
			return true;
		}
		if (x > Game.WIDTH - width) {
			Game.leftScore++;
			return true;
		}
		return false;
	}
	
	public void incrementSpeed() {
		if (Math.abs(vx) < MAX_VELOCITY) {
			if (vx < 0)
				vx -= incr;
			else
				vx += incr;
		}
	}
	
	public void render(Graphics2D g) {
		g.setFont(new Font("Arial", Font.BOLD, 24));
		int time = (int) ((System.currentTimeMillis() - startTime)/1000L);
		if (time < 3) {
			g.drawString("" + (3 - time), Game.WIDTH/2, Game.HEIGHT/2 - 20);
		}
		g.fillOval((int)x, (int)y, width, height);
	}
	
	@Override
	public String toString() {
		return "bot";
	}
	
}
