package com.zeroqualitygames.pong;

import javax.swing.JFrame;


public class GameMain extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Game game = new Game();
	private int frameWidth = 6, frameHeight = 28;
	
	public GameMain(String name) {
		super(name);
		
		add(game);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(Game.WIDTH + frameWidth, Game.HEIGHT + frameHeight);
		setVisible(true);
		setResizable(false);
		setLocationRelativeTo(null);
		
		game.start();
	}
	
	public static void main (String[] args) {
		new GameMain("Pong by Zero Quality Games");
	}
	
}
